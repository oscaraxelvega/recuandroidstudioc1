package com.example.recuperacionc1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private Button btnIMC, btnRegresar, btnConvertidor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        initComponents();
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().matches(""))
                    Toast.makeText(MainActivity.this, "Ingrese el nombre", Toast.LENGTH_SHORT).show();
                else{
                    String nombre = txtUsuario.getText().toString();
                    Intent intent = new Intent(getApplicationContext(), ImcActivity.class);
                    intent.putExtra("usuario", nombre);
                    startActivity(intent);
                }
            }
        });
        btnConvertidor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtUsuario.getText().toString().matches(""))
                    Toast.makeText(MainActivity.this, "Ingrese el nombre", Toast.LENGTH_SHORT).show();
                else{
                    String nombre = txtUsuario.getText().toString();
                    Intent intent = new Intent(getApplicationContext(), ConvertidorActivity.class);
                    intent.putExtra("usuario", nombre);
                    startActivity(intent);
                }
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initComponents(){
        txtUsuario = findViewById(R.id.txtUsuario);
        btnIMC = findViewById(R.id.btnIMC);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnConvertidor= findViewById(R.id.btnConvertidor);
    }
}